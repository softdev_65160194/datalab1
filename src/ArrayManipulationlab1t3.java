public class ArrayManipulationlab1t3 {
    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        // Start from the end of the merged array
        int i = m - 1;  // Pointer for nums1
        int j = n - 1;  // Pointer for nums2
        int k = m + n - 1;  // Pointer for the merged array nums1

        // Compare elements from both arrays and place them in the correct order in nums1
        System.out.println(i);
        System.out.println(j);
        System.out.println(k);
        while (i >= 0 && j >= 0) {
            if (nums1[i] >= nums2[j]) {
                nums1[k] = nums1[i];
                i--;
            } else {
                nums1[k] = nums2[j];
                j--;
            }
            k--;
        }

        // If there are any remaining elements in nums2, copy them to nums1
        while (j >= 0) {
            nums1[k] = nums2[j];
            j--;
            k--;
        }
    }

    public static void main(String[] args) {
        int[] nums1 = {1, 2, 3, 0, 0, 0};
        int m = 3;
        int[] nums2 = {2, 5, 6};
        int n = 3;

        merge(nums1, m, nums2, n);

        // Print the merged array
        for (int count = 0;count < nums1.length;count++) {
            System.out.print(nums1[count] + " ");
        }
        // Output: 1 2 2 3 5 6
    }
}
