import java.util.Arrays;
//1
public class ArrayManipulation {
    public static void main(String[] args) {
        //2
        int[] numbers = {5, 8, 3, 2, 7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        double[] values = new double[4];
        //3
        for(int i = 0;i < numbers.length;i++){
            System.out.println(numbers[i]);
        }
        //4
        for(int i = 0;i < names.length;i++){
            System.out.println(names[i]);
        }
        //5
        values[0] = 1.0;
        values[1] = 2.1;
        values[2] = 3.2;
        values[3] = 4.1;
        //6
        int sum = 0;
        for(int i = 0;i < numbers.length;i++){
            sum = sum + numbers[i];
        }
        System.out.println(sum);
        //7
        double high = 0;
        for(int i = 0;i < 4;i++){
            if(high < values[i]){
                high = values[i];
            }
        }
        System.out.println(high);
        //8
        String[] reverse = new String[4];
        for(int i = 0,j=3;i < 4;i++,j--){
            reverse[i] = names[j];
        }
        for(int i = 0;i < 4;i++){
            System.out.println(reverse[i]);
        }
        //9
        Arrays.sort(numbers);
        for(int i = 0;i < numbers.length;i++){
            System.out.print(numbers[i]);
            System.out.print(" ");
        }
    }
}
